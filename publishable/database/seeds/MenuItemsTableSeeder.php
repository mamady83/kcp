<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;

class MenuItemsTableSeeder extends Seeder{

    public function run(){

        $menu = Menu::where('name', 'admin')->firstOrFail();

        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title' => __('voyager::seeders.menu_items.dashboard'),
            'url' => '',
            'route' => 'voyager.dashboard',
        ]);
        if(!$menuItem->exists){
            $menuItem->fill([
                'target' => '_self',
                'icon_class' => 'Building, Construction/bank-building.svg',
                'color' => null,
                'parent_id' => null,
                'order' => 1,
            ])->save();
        }

        $toolsMenuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title' => __('voyager::seeders.menu_items.tools'),
            'url' => '',
        ]);
        if(!$toolsMenuItem->exists){
            $toolsMenuItem->fill([
                'target' => '_self',
                'icon_class' => 'Construction, Tools/hammer-screwdriver.svg',
                'color' => null,
                'parent_id' => null,
                'order' => 9,
            ])->save();
        }

        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title' => __('voyager::seeders.menu_items.menu_builder'),
            'url' => '',
            'route' => 'voyager.menus.index',
        ]);
        if(!$menuItem->exists){
            $menuItem->fill([
                'target' => '_self',
                'icon_class' => 'Type, Paragraph, Character/list-paragraph-dots-circle.svg',
                'color' => null,
                'parent_id' => $toolsMenuItem->id,
                'order' => 10,
            ])->save();
        }

        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title' => __('voyager::seeders.menu_items.meta_builder'),
            'url' => '',
            'route' => 'voyager.metas.index',
        ]);
        if(!$menuItem->exists){
            $menuItem->fill([
                'target' => '_self',
                'icon_class' => 'Interface Essential/text-item-list.svg',
                'color' => null,
                'parent_id' => $toolsMenuItem->id,
                'order' => 15,
            ])->save();
        }


        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title' => __('voyager::seeders.menu_items.media'),
            'url' => '',
            'route' => 'voyager.media.index',
        ]);
        if(!$menuItem->exists){
            $menuItem->fill([
                'target' => '_self',
                'icon_class' => 'Photo Edit/Images, Photo, Edit.svg',
                'color' => null,
                'parent_id' => $toolsMenuItem->id,
                'order' => 5,
            ])->save();
        }

        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title' => __('voyager::seeders.menu_items.users'),
            'url' => '',
            'route' => 'voyager.users.index',
        ]);
        if(!$menuItem->exists){
            $menuItem->fill([
                'target' => '_self',
                'icon_class' => 'Interface Essential/users-circle.svg',
                'color' => null,
                'parent_id' => $toolsMenuItem->id,
                'order' => 3,
            ])->save();
        }

        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title' => __('voyager::seeders.menu_items.roles'),
            'url' => '',
            'route' => 'voyager.roles.index',
        ]);
        if(!$menuItem->exists){
            $menuItem->fill([
                'target' => '_self',
                'icon_class' => 'Interface Essential/lock.8.svg',
                'color' => null,
                'parent_id' => $toolsMenuItem->id,
                'order' => 2,
            ])->save();
        }


       /* $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title' => __('voyager::seeders.menu_items.database'),
            'url' => '',
            'route' => 'voyager.database.index',
        ]);
        if(!$menuItem->exists){
            $menuItem->fill([
                'target' => '_self',
                'icon_class' => 'voyager-data',
                'color' => null,
                'parent_id' => $toolsMenuItem->id,
                'order' => 11,
            ])->save();
        }*/


         $menuItem = MenuItem::firstOrNew([
             'menu_id' => $menu->id,
             'title' => __('voyager::seeders.menu_items.chart'),
             'url' => '',
             'route' => 'voyager.chart.index',
         ]);
         if(!$menuItem->exists){
             $menuItem->fill([
                 'target' => '_self',
                 'icon_class' => 'Business, Products/Business, Chart.1.svg',
                 'color' => null,
                 'parent_id' => $toolsMenuItem->id,
                 'order' => 11,
             ])->save();
         }

        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title' => __('voyager::seeders.menu_items.compass'),
            'url' => '',
            'route' => 'voyager.compass.index',
        ]);
        if(!$menuItem->exists){
            $menuItem->fill([
                'target' => '_self',
                'icon_class' => 'Interface Essential/compass-map-circle.1.svg',
                'color' => null,
                'parent_id' => $toolsMenuItem->id,
                'order' => 12,
            ])->save();
        }

        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title' => __('voyager::seeders.menu_items.bread'),
            'url' => '',
            'route' => 'voyager.bread.index',
        ]);
        if(!$menuItem->exists){
            $menuItem->fill([
                'target' => '_self',
                'icon_class' => 'Food/bread 2.svg',
                'color' => null,
                'parent_id' => $toolsMenuItem->id,
                'order' => 13,
            ])->save();
        }

        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title' => __('voyager::seeders.menu_items.settings'),
            'url' => '',
            'route' => 'voyager.settings.index',
        ]);
        if(!$menuItem->exists){
            $menuItem->fill([
                'target' => '_self',
                'icon_class' => 'Interface Essential/setting-1.svg',
                'color' => null,
                'parent_id' => null,
                'order' => 14,
            ])->save();
        }
    }
}
