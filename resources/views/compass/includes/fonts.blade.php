
@php

    $dir = __DIR__.'/../../../vendor/karo/kcp/publishable/assets/panel_icon';
    $files = scandir($dir);
    $i=0;

@endphp

@foreach ($files as $key => $value)

    @php $path = realpath($dir . DIRECTORY_SEPARATOR . $value); @endphp
    @if (!is_dir($path))

    @elseif ($value != "." && $value != "..")

        @php $files1 = scandir($path); @endphp
        <h6 style="text-align: left; margin-top: 20px">
            {{ $value }}

        </h6>
        <hr>
        @foreach ($files1 as $key1 => $value1)

            @php $path1 = realpath($path . DIRECTORY_SEPARATOR . $value1); @endphp

            @if (!is_dir($path1))
                <div class="padding-item col-lg-3 col-md-3 col-sm-4 col-6"
                     onclick="myFunction('{{$value . "/" . $value1}}' , {{$i}})">
                    <div class="icons-item flex-box position-relative">
                        <div class="copy-item">
                            <a class="copy">
                                {{ __('voyager::generic.copy') }}
    
                            </a>
                        </div>
                        <img style="height: 25px;margin-right: 12px;margin-left: 12px;"
                             src="{{ voyager_asset('panel_icon/' . $value . "/" . $value1) }}">
                        {{  $value1  }}
                    </div>
                </div>
                @php $i++; @endphp
            @endif
        @endforeach
    @endif
@endforeach



<script>
    function myFunction(value, index) {

        navigator.clipboard.writeText(value);
        document.getElementsByClassName("copy-item")[index].style.display = "block"
        setTimeout(function() {
             document.getElementsByClassName("copy-item")[index].style.display = "none"
        }, 3000); // <-- time in milliseconds
    }

</script>


{{--<script>--}}
{{--    (function () {--}}
{{--        var glyphs, i, len, ref;--}}

{{--        ref = document.getElementsByClassName('glyphs');--}}
{{--        for (i = 0, len = ref.length; i < len; i++) {--}}
{{--            glyphs = ref[i];--}}
{{--            glyphs.addEventListener('click', function (event) {--}}
{{--                if (event.target.tagName === 'INPUT') {--}}
{{--                    return event.target.select();--}}
{{--                }--}}
{{--            });--}}
{{--        }--}}
{{--    }).call(this);--}}
{{--</script>--}}
