<div>
    <input type="text" class="DatepickerKcp_{{ $row->field }} @if(Auth::user()->Locale =='fa') rtlMode @endif"
           name="{{ $row->field }}"
           placeholder="{{ $row->getTranslatedAttribute('display_name') }}"
           id="DatepickerIDKcp_{{ $row->field }}"

           value="@if(isset($dataTypeContent->{$row->field}))
           {{ \Carbon\Carbon::parse(old($row->field, $dataTypeContent->{$row->field}))->format('Y-m-d') }}
           @else{{old($row->field)}}@endif"
            autocomplete="off"
        >




    <img class="black-calender" src="{{voyager_asset('icon/black-calender.svg')}}">
    <img class="delet-value" src="{{voyager_asset('icon/delet-value.svg')}}">

</div>


<script>
    $(document).ready(function (){

        $(".DatepickerKcp_{{ $row->field }}").click(function (e){
            e.preventDefault();
        })

        $('.DatepickerKcp_{{ $row->field }}').datepicker({
            controlType: 'select',
            oneLine: true,
            timeFormat: 'HH:mm',
            timepicker: false,

        });
    })
</script>
