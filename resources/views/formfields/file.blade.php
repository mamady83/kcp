{{--<input @if($row->required == 1 && !isset($dataTypeContent->{$row->field})) required @endif type="file" name="{{ $row->field }}[]" multiple="multiple">--}}

<div class="padding-item margin-bottom col-lg-12 col-md-12 col-sm-12">
    <div class="input-row">

        <label id="fileimage" for="upload-file_{{ $row->field }}" class="upload-file menu-items flex-box">
        <span class="flex-box">
            <img src="{{ voyager_asset('icon/upload-image.svg') }}">
             {{ __('voyager::generic.upload_file') }}
        </span>
            <p>
                {{ __('voyager::generic.file_selection') }}
            </p>

            <input id="upload-file_{{ $row->field }}" class="upload-fileInput" @if($row->required == 1 && !isset($dataTypeContent->{$row->field})) required
                   @endif type="file" name="{{ $row->field }}[]" multiple="multiple" hidden/>
            {{--        <input type="file" class="" multiple="" hidden="">--}}
        </label>
    </div>
    <div class="upload-file-row file-row margin-bottom">

        @if(isset($dataTypeContent->{$row->field}) && $dataTypeContent->{$row->field} != '[]')

            <div class="acsses  files">

                <div class="flex-box">
                    @if(json_decode($dataTypeContent->{$row->field}) !== null)
                        @foreach(json_decode($dataTypeContent->{$row->field}) as $file)

                            <div data-field-name="{{ $row->field }}">
                                <img class="close-tag voyager-x remove-multi-file" src="{{ voyager_asset('icon%2Fclos.svg')  }}">
                                <a class="fileType" target="_blank" style="color: inherit"
                                   href="{{ Storage::disk(config('voyager.storage.disk'))->url($file->download_link) ?: '' }}"
                                   data-file-name="{{ $file->original_name }}"
                                   data-id="{{ $dataTypeContent->getKey() }}">
                                    {{ $file->original_name ?: '' }}
                                </a>
                            </div>

                        @endforeach
                    @else

                        <div data-field-name="{{ $row->field }}">
                        <img class="close-tag voyager-x remove-single-file" src="{{ voyager_asset('icon%2Fclos.svg')  }}">
                            <a class="fileType" target="_blank" style="color: inherit"
                               href="{{ Storage::disk(config('voyager.storage.disk'))->url($dataTypeContent->{$row->field}) }}"
                               data-file-name="{{ $dataTypeContent->{$row->field} }}"
                               data-id="{{ $dataTypeContent->getKey() }}">

                                {{ __('voyager::generic.download') }}
                            </a>
{{--                            <a href="#" class="voyager-x remove-single-file"></a>--}}
                        </div>
                    @endif

                </div>

                <div class="flex-box upload-prosses">

                </div>
            </div>
        @endif

    </div>
</div>
