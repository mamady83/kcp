<div class="padding-item margin-bottom col-lg-12 col-md-12 col-sm-12">
    <div class="input-row">
        <label>
            {{ __('voyager::generic.upload_photo') }}
        </label>
        <label id="fileimage" for="upload-imge" class="upload-file menu-items flex-box">
        <span class="flex-box">
            <img src="{{ voyager_asset('icon/upload-image.svg') }}">
             {{ __('voyager::generic.upload_photo') }}
        </span>
            <p>
                {{ __('voyager::generic.photo_selection') }}

            </p>

            <input @if($row->required == 1 && !isset($dataTypeContent->{$row->field})) required @endif type="file"
                   name="{{ $row->field }}" id="upload-imge" hidden accept="image/*">
            {{--        <input type="file" class="" multiple="" hidden="">--}}
        </label>
    </div>
    <div class="upload-image-row file-row margin-bottom">
    </div>
</div>

