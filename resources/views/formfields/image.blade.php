

<div class="padding-item margin-bottom col-lg-12 col-md-12 col-sm-12 "><!--imageupload-->
    <div class="input-row">
        <label id="fileimage" for="upload-imge-{{ $row->field }}" class="upload-file menu-items flex-box">
        <span class="flex-box">
            <img src="{{ voyager_asset('icon/upload-image.svg') }}">
             {{ __('voyager::generic.upload_photo') }}

        </span>
            <p>
                {{ __('voyager::generic.photo_selection') }}
            </p>

            <input @if($row->required == 1 && !isset($dataTypeContent->{$row->field})) required @endif type="file"
                   name="{{ $row->field }}" id="upload-imge-{{ $row->field }}" class="uploadImageInput"
                   data-row-id="{{ $row->field }}"
                   data-preview="preview{{ $row->field }}Pic" hidden accept="image/*">
        </label>
    </div>
    <div class="upload-image-row file-row margin-bottom preview{{ $row->field }}Pic">


        @if($dataTypeContent->{$row->field})

            <div class="acsses image-file files thumbnail" id="files{{ $row->field }}" data-field-name="{{ $row->field }}">

                <a href="#" class="remove-image-Single" style="position:absolute;z-index: 100;left: 0;">
                    <img src="{{ voyager_asset('icon%2Fclos.svg') }}" style="margin: unset">
                </a>
                <div class="flex-box">
                    <div class="image-box">
                        <img id="upload-image-item"
                             src="@if( !filter_var($dataTypeContent->{$row->field}, FILTER_VALIDATE_URL)){{ Voyager::image( $dataTypeContent->{$row->field} ) }}
                             @else{{ $dataTypeContent->{$row->field} }}@endif"
                             data-file-name="{{ $dataTypeContent->{$row->field} }}" data-id="{{ $dataTypeContent->getKey() }}"
                             onclick="window.open('{{ \TCG\Voyager\Facades\Voyager::image($dataTypeContent->{$row->field} ) }}', '_blank').focus();">
                    </div>
                </div>
                <div class="flex-box upload-prosses"></div>



            </div>
        @endif
    </div>
</div>
