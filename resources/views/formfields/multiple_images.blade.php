<div class="padding-item margin-bottom col-lg-12 col-md-12 col-sm-12">
    <div class="input-row">

        <label id="fileimage" for="upload-imge" class="upload-file menu-items flex-box">
            <span class="flex-box">
            <img src="{{ voyager_asset('icon/upload-image.svg') }}">
            {{ __('voyager::generic.upload_photo') }}
        </span>
            <p>
                {{ __('voyager::generic.photo_selection') }}

            </p>

            <input @if($row->required == 1 && !isset($dataTypeContent->{$row->field})) required @endif type="file"
                   name="{{ $row->field }}[]" multiple="multiple" id="upload-imge" class="upload-image-in" hidden accept="image/*">
            {{--        <input type="file" class="" multiple="" hidden="">--}}
        </label>
    </div>
    <div class="upload-image-row file-row margin-bottom">
        @if(isset($dataTypeContent->{$row->field}))
            <?php $images = json_decode($dataTypeContent->{$row->field}); ?>
            @if($images != null)
                @foreach($images as $image)

                    <div class="acsses image-file files">
                        <div class="flex-box"  data-field-name="{{ $row->field }}">
                            <img class="close-tag img_settings_container removeMultiImage" src="{{ voyager_asset('icon/clos.svg') }}">
                            <div class="image-box" data-field-name="{{ $row->field }}">
                                <img src="{{ Voyager::image( $image ) }}" id="upload-image-item" data-file-name="{{ $image }}"
                                     data-id="{{ $dataTypeContent->getKey() }}">
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        @endif
    </div>
</div>

