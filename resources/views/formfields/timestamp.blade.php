<div>

    @php
        $traits = class_uses($dataTypeContent);
        $isjalali = in_array("App\Http\Traits\JalaliDate",$traits);
    @endphp
    @if(!$isjalali)

        <input @if($row->required == 1) required @endif type="text" class="hDeb timeAndDatepicker_{{ $row->field }}" name="{{ $row->field }}" value="{{ $dataTypeContent->{$row->field} }}" autocomplete="off"/>


        {{-- <input @if($row->required == 1) required @endif type="datetime-local"
               class="form-control  datePickerInput @if(Auth::user()->Locale =='fa') rtlMode @endif"
               name="{{ $row->field }}"
               value="{{ $dataTypeContent->{$row->field} }}"> --}}
        {{--           value="@if(isset($dataTypeContent->{$row->field})){{ \Carbon\Carbon::parse(old($row->field, $dataTypeContent->{$row->field}))->format('m/d/Y g:i A') }}@else{{old($row->field)}}@endif">--}}
    @else

        <input @if($row->required == 1) required @endif type="text"
               class="form-control date datePickerInput @if(Auth::user()->Locale =='fa') rtlMode @endif"
               name="{{ $row->field }}"
               value="@if(isset($dataTypeContent->{$row->field})){{ $dataTypeContent->geodate }}@else{{old($row->geodate)}}@endif">
    @endif

    <img class="black-calender" src="{{voyager_asset('icon/black-calender.svg')}}">
    <img class="delet-value" src="{{voyager_asset('icon/delet-value.svg')}}">
</div>

<script>
    $(document).ready(function (){

        $(".timeAndDatepicker_{{ $row->field }}").click(function (e){
            e.preventDefault();
        })

        $('.timeAndDatepicker_{{ $row->field }}').datetimepicker({
            controlType: 'select',
            oneLine: true,
            timeFormat: 'HH:mm',
            timepicker: false
        });
    })
</script>
