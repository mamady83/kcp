@extends('voyager::master')

@section('page_title', __('voyager::generic.media'))

@section('content')
    <div class="page-content container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                <nav class="navigation">
                    <div class="right-top-nav flex-box">
                        <a class="flex-box back">
                            <img src="{{ voyager_asset('icon/back.svg') }}">
                            برگشت
                        </a>
                        <a class="flex-box add-new">
                            <img src="{{ voyager_asset('icon/change.svg') }}">
                            <p>
                                ثبت تغییرات
                            </p>
                        </a>
                    </div>
                </nav>
                <div class="more-row second-nav">
                    <div class="row">
                        <div class=" col-lg-6 col-md-6 col-sm-12">
                            <div class="flex-box title-details">
                                <p class="blue-text">
                                    کتابخانه چند رسانه ای
                                </p>
                                <img src="{{ voyager_asset('icon/blue-arrow.svg') }}">
                                <p class="blue-text">
                                    فولدر آلبوم ها
                                </p>
                                <img src="{{ voyager_asset('icon/blue-arrow.svg') }}">
                                <p>
                                    18 تیر 1400
                                </p>
                            </div>
                        </div>
                        <div class=" col-lg-6 col-md-6 col-sm-12">
                            <h5 class="title">
                                {{ __('voyager::generic.media') }}
                            </h5>
                        </div>
                    </div>
                </div>



                <div class="clear"></div>
                <div id="filemanager">
                    <media-manager
                        base-path="{{ config('voyager.media.path', '/') }}"
                        :show-folders="{{ config('voyager.media.show_folders', true) ? 'true' : 'false' }}"
                        :allow-upload="{{ config('voyager.media.allow_upload', true) ? 'true' : 'false' }}"
                        :allow-move="{{ config('voyager.media.allow_move', true) ? 'true' : 'false' }}"
                        :allow-delete="{{ config('voyager.media.allow_delete', true) ? 'true' : 'false' }}"
                        :allow-create-folder="{{ config('voyager.media.allow_create_folder', true) ? 'true' : 'false' }}"
                        :allow-rename="{{ config('voyager.media.allow_rename', true) ? 'true' : 'false' }}"
                        :allow-crop="{{ config('voyager.media.allow_crop', true) ? 'true' : 'false' }}"
                        :details="{{ json_encode(['thumbnails' => config('voyager.media.thumbnails', []), 'watermark' => config('voyager.media.watermark', (object)[])]) }}"
                        ></media-manager>
                </div>
            </div><!-- .row -->
        </div><!-- .col-md-12 -->
    </div><!-- .page-content container-fluid -->
@stop

@section('javascript')
<script>
new Vue({
    el: '#filemanager'
});
</script>
@endsection
