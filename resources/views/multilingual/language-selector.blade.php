@if (isset($isModelTranslatable) && $isModelTranslatable)
    <div class="language-selector">
        <div class="btn-group btn-group-sm" role="group" data-toggle="buttons"
             style="display: block; text-align: right; margin-right: 10px">
            @foreach(config('voyager.multilingual.locales') as $lang)
                <label class="btn btn-primary{{ ($lang === config('voyager.multilingual.default')) ? " active" : "" }} selectLangLabel"
                       style="margin: 4px;border-radius: 4px; background: #26D882 ; border-color: #26D882; padding: 5px">
                    <input type="button" name="i18n_selector" id="{{$lang}}" autocomplete="off"{{ ($lang === config('voyager.multilingual.default')) ? ' checked="checked"' : '' }} hidden/>
                    {{ strtoupper($lang) }}
                </label>
            @endforeach
        </div>
    </div>
@endif

