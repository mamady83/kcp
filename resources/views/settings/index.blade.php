@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.__('voyager::generic.settings'))

@section('css')
    <style>
        .panel-actions .voyager-trash {
            cursor: pointer;
        }

        .panel-actions .voyager-trash:hover {
            color: #e94542;
        }

        .settings .panel-actions {
            right: 0px;
        }

        .panel hr {
            margin-bottom: 10px;
        }

        .panel {
            padding-bottom: 15px;
        }

        .sort-icons {
            font-size: 21px;
            color: #ccc;
            position: relative;
            cursor: pointer;
        }

        .sort-icons:hover {
            color: #37474F;
        }

        .voyager-sort-desc {
            margin-right: 10px;
        }

        .voyager-sort-asc {
            top: 10px;
        }

        .page-title {
            margin-bottom: 0;
        }

        .panel-title code {
            border-radius: 30px;
            padding: 5px 10px;
            font-size: 11px;
            border: 0;
            position: relative;
            top: -2px;
        }

        .modal-open .settings .select2-container {
            z-index: 9 !important;
            width: 100% !important;
        }

        .new-setting {
            text-align: center;
            width: 100%;
            margin-top: 20px;
        }

        .new-setting .panel-title {
            margin: 0 auto;
            display: inline-block;
            color: #999fac;
            font-weight: lighter;
            font-size: 13px;
            background: #fff;
            width: auto;
            height: auto;
            position: relative;
            padding-right: 15px;
        }

        .settings .panel-title {
            padding-left: 0px;
            padding-right: 0px;
        }

        .new-setting hr {
            margin-bottom: 0;
            position: absolute;
            top: 7px;
            width: 96%;
            margin-left: 2%;
        }

        .new-setting .panel-title i {
            position: relative;
            top: 2px;
        }

        .new-settings-options {
            display: none;
            padding-bottom: 10px;
        }

        .new-settings-options label {
            margin-top: 13px;
        }

        .new-settings-options .alert {
            margin-bottom: 0;
        }

        #toggle_options {
            clear: both;
            float: right;
            font-size: 12px;
            position: relative;
            margin-top: 15px;
            margin-right: 5px;
            margin-bottom: 10px;
            cursor: pointer;
            z-index: 9;
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        .new-setting-btn {
            margin-right: 15px;
            position: relative;
            margin-bottom: 0;
            top: 5px;
        }

        .new-setting-btn i {
            position: relative;
            top: 2px;
        }

        textarea {
            min-height: 120px;
        }

        textarea.hidden {
            display: none;
        }

        .voyager .settings .nav-tabs {
            background: none;
            border-bottom: 0px;
        }

        .voyager .settings .nav-tabs .active a {
            border: 0px;
        }

        .select2 {
            width: 100% !important;
            border: 1px solid #f1f1f1;
            border-radius: 3px;
        }

        .voyager .settings input[type=file] {
            width: 100%;
        }

        .settings .select2 {
            margin-left: 10px;
        }

        .settings .select2-selection {
            height: 32px;
            padding: 2px;
        }

        .voyager .settings .nav-tabs > li {
            margin-bottom: -1px !important;
        }

        .voyager .settings .nav-tabs a {
            text-align: center;
            background: #f8f8f8;
            border: 1px solid #f1f1f1;
            position: relative;
            top: -1px;
            border-bottom-left-radius: 0px;
            border-bottom-right-radius: 0px;
        }

        .voyager .settings .nav-tabs a i {
            display: block;
            font-size: 22px;
        }

        .tab-content {
            background: #ffffff;
            border: 1px solid transparent;
        }

        .tab-content > div {
            padding: 10px;
        }

        .settings .no-padding-left-right {
            padding-left: 0px;
            padding-right: 0px;
        }

        .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
            background: #fff !important;
            color: #62a8ea !important;
            border-bottom: 1px solid #fff !important;
            top: -1px !important;
        }

        .nav-tabs > li a {
            transition: all 0.3s ease;
        }


        .nav-tabs > li.active > a:focus {
            top: 0px !important;
        }

        .voyager .settings .nav-tabs > li > a:hover {
            background-color: #fff !important;
        }
    </style>
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-settings"></i> {{ __('voyager::generic.settings') }}
    </h1>
@stop

@section('content')
    <div class="row">
        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
            <nav class="navigation">
                <div class="right-top-nav flex-box">
                    <a class="flex-box back" href="{{ url()->previous() }}">
                        <img src="{{voyager_asset('icon/back.svg')}}">
                        {{ __('voyager::generic.return_to_list') }}

                    </a>

                    <a class="flex-box add-new" onclick="document.getElementById('setting_form').submit()">
                        <img src="{{voyager_asset('icon/change.svg')}}">
                        <p>
                            {{ __('voyager::settings.save') }}
                        </p>
                    </a>

                </div>
            </nav>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="row">
                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                            <h5 class="title">
                                {{ __('voyager::generic.settings') }}
                            </h5>
                        </div>
                        @include('voyager::alerts')
                        @if(config('voyager.show_dev_tips'))
                            <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                                <div class="box">
                                    <div class=" flex-box edit-massage">
                                        <div class="yellow flex-box  image-box">
                                            <img src="{{voyager_asset('icon/Help.svg')}}">
                                        </div>
                                        <p>
                                            {{ __('voyager::settings.usage_help') }}
                                            <a class="acsses">
                                                setting('group.key')
                                            </a>
                                        </p>

                                    </div>
                                </div>
                            </div>
                        @endif

                        <form action="{{ route('voyager.settings.update') }}" method="POST"
                              enctype="multipart/form-data" id="setting_form">
                            {{ method_field("PUT") }}
                            {{ csrf_field() }}
                            <input type="hidden" name="setting_tab" class="setting_tab" value="{{ $active }}"/>
                            <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                                <div class="tabs">
                                    <ul class="tabs__button-group">
                                        @foreach($settings as $group => $setting)

                                            <li>
                                                <button class="tabs__toggle @if($group == $active) active @endif"
                                                        data-tab="{{ $loop->index }}" type="button">
                                                    {{ $group }}
                                                </button>
                                            </li>
                                        @endforeach

                                    </ul>

                                    <ul class="tabs__container box setting-row"
                                        id="{{ \Illuminate\Support\Str::slug($group) }}">

                                        @foreach($settings as $group => $group_settings)
                                            <li class="tabs__tab-panel @if($group == $active) active @endif"
                                                data-tab="{{$loop->index}}">
                                                @foreach($group_settings as $setting)
                                                    <div class="row Breads margin-bottom">
                                                        <div class=" padding-item col-lg-12 col-md-12 col-sm-12">
                                                            <div class="title-rows flex-box justify-content-between">
                                                                <p>
                                                                    @if(config('voyager.show_dev_tips'))
                                                                        <code>setting('{{ $setting->key }}')</code>
                                                                    @endif
                                                                    {{ $setting->display_name }}

                                                                </p>
                                                                <div class="left-item">
                                                                    <p>
                                                                        {{ __('voyager::generic.group') }}
                                                                    </p>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class=" col-lg-12 col-md-12 col-sm-12">
                                                            <div draggable="true" class="menu-items flex-box" style="align-items: flex-start">
                                                                <div class="dropdown-row input-row">
                                                                    @if ($setting->type == "text")
                                                                        <input type="text" class="form-control"
                                                                               name="{{ $setting->key }}"
                                                                               value="{{ $setting->value }}">
                                                                    @elseif($setting->type == "text_area")
                                                                        <textarea class="form-control"
                                                                                  name="{{ $setting->key }}">{{ $setting->value ?? '' }}</textarea>
                                                                    @elseif($setting->type == "rich_text_box")
                                                                        <textarea class="form-control richTextBox"
                                                                                  name="{{ $setting->key }}">{{ $setting->value ?? '' }}</textarea>
                                                                    @elseif($setting->type == "image" || $setting->type == "file")
                                                                        <div class="padding-item margin-bottom col-lg-12 col-md-12 col-sm-12">
                                                                            <div class="input-row">

                                                                                <label id="fileimage"
                                                                                       for="upload-file_{{ $setting->key }}"
                                                                                       class="upload-file menu-items flex-box">
                                                                                    <span class="flex-box">
                                                                                    <img
                                                                                        src="{{ voyager_asset('icon/upload-image.svg') }}">

                                                                                     {{ __('voyager::generic.upload_file') }}

                                                                                    </span>
                                                                                    <p style="color: var(--blue_btn_bg)!important;">

                                                                                        {{ __('voyager::generic.file_selection') }}
                                                                                    </p>

                                                                                    <input
                                                                                        id="upload-file_{{ $setting->key }}"
                                                                                        class="upload-fileInput"
                                                                                        type="file"
                                                                                        name="{{ $setting->key }}"
                                                                                        multiple="multiple" hidden="">

                                                                                </label>
                                                                            </div>
                                                                            <div class="upload-file-row margin-bottom">

                                                                            </div>

                                                                        </div>

                                                                        @if(isset( $setting->value ) && !empty( $setting->value ) && Storage::disk(config('voyager.storage.disk'))->exists($setting->value))
                                                                            <div class="acsses image-file files thumbnail">

                                                                                <a href="{{ route('voyager.settings.delete_value', $setting->id) }}"
                                                                                   class=" delete_value"
                                                                                   style="position:absolute;z-index: 100;left: 0;">
                                                                                    <img
                                                                                        src="{{ voyager_asset('icon%2Fclos.svg') }}"
                                                                                        style="margin: unset">
                                                                                </a>
                                                                                <div class="flex-box">
                                                                                    <div class="image-box">
                                                                                        <img id="upload-image-item"
                                                                                             src="{{ Storage::disk(config('voyager.storage.disk'))->url($setting->value) }}">
                                                                                    </div>
                                                                                </div>
                                                                                <div
                                                                                    class="flex-box upload-prosses"></div>
                                                                            </div>

                                                                        @elseif($setting->type == "file" && isset( $setting->value ))
                                                                            @if(json_decode($setting->value) !== null)
                                                                                @foreach(json_decode($setting->value) as $file)
                                                                                    <div class="acsses  files">
                                                                                        <div class="flex-box">
                                                                                            <div>
                                                                                                <a href="{{ route('voyager.settings.delete_value', $setting->id) }}"
                                                                                                   class=" delete_value">
                                                                                                    <img
                                                                                                        class="close-tag voyager-x remove-multi-file"
                                                                                                        src="{{ voyager_asset('icon%2Fclos.svg')  }}">
                                                                                                </a>
                                                                                                <a class="fileType"
                                                                                                   target="_blank"
                                                                                                   style="color: inherit"
                                                                                                   href="{{ Storage::disk(config('voyager.storage.disk'))->url($file->download_link) }}">
                                                                                                    {{ $file->original_name }}
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                @endforeach
                                                                            @endif
                                                                        @endif

                                                                    @elseif($setting->type == "select_dropdown")
                                                                        <?php $options = json_decode($setting->details); ?>
                                                                        <?php $selected_value = (isset($setting->value) && !empty($setting->value)) ? $setting->value : NULL; ?>
                                                                        <select class="form-control"
                                                                                name="{{ $setting->key }}">
                                                                            <?php $default = (isset($options->default)) ? $options->default : NULL; ?>
                                                                            @if(isset($options->options))
                                                                                @foreach($options->options as $index => $option)
                                                                                    <option value="{{ $index }}"
                                                                                            @if($default == $index && $selected_value === NULL) selected="selected"
                                                                                            @endif @if($selected_value == $index) selected="selected" @endif>{{ $option }}</option>
                                                                                @endforeach
                                                                            @endif
                                                                        </select>

                                                                    @elseif($setting->type == "radio_btn")
                                                                        <?php $options = json_decode($setting->details); ?>
                                                                        <?php $selected_value = (isset($setting->value) && !empty($setting->value)) ? $setting->value : NULL; ?>
                                                                        <?php $default = (isset($options->default)) ? $options->default : NULL; ?>
                                                                        <ul class="radio">
                                                                            @if(isset($options->options))
                                                                                @foreach($options->options as $index => $option)
                                                                                    <li>
                                                                                        <input type="radio"
                                                                                               id="option-{{ $index }}"
                                                                                               name="{{ $setting->key }}"
                                                                                               value="{{ $index }}"
                                                                                               @if($default == $index && $selected_value === NULL) checked
                                                                                               @endif @if($selected_value == $index) checked @endif>
                                                                                        <label
                                                                                            for="option-{{ $index }}">{{ $option }}</label>
                                                                                        <div class="check"></div>
                                                                                    </li>
                                                                                @endforeach
                                                                            @endif
                                                                        </ul>
                                                                    @elseif($setting->type == "checkbox")
                                                                        <?php $options = json_decode($setting->details); ?>
                                                                        <?php $checked = (isset($setting->value) && $setting->value == 1) ? true : false; ?>
                                                                        @if (isset($options->on) && isset($options->off))
                                                                            <input type="checkbox"
                                                                                   name="{{ $setting->key }}"
                                                                                   class="toggleswitch"
                                                                                   @if($checked) checked
                                                                                   @endif data-on="{{ $options->on }}"
                                                                                   data-off="{{ $options->off }}">
                                                                        @else
                                                                            <input type="checkbox"
                                                                                   name="{{ $setting->key }}"
                                                                                   @if($checked) checked
                                                                                   @endif class="toggleswitch">
                                                                        @endif
                                                                    @endif
                                                                </div>
                                                                <div
                                                                    class="left-item flex-box  justify-content-between" style="margin-top: 10px;">
                                                                    <div class="input-row dropdown-row">
                                                                        <div class="dropdown custom-selects">
                                                                            <select name="{{ $setting->key }}_group">
                                                                                @foreach($groups as $group)
                                                                                    <option
                                                                                        value="{{ $group }}" {!! $setting->group == $group ? 'selected' : '' !!}>{{ $group }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <a class="account-number flex-box panel-actions ">
                                                                        @can('delete', Voyager::model('Setting'))
                                                                            <img
                                                                                src="{{voyager_asset('icon/trash.svg')}}"
                                                                                class="voyager-trash"
                                                                                data-id="{{ $setting->id }}"
                                                                                data-display-key="{{ $setting->key }}"
                                                                                data-display-name="{{ $setting->display_name }}">
                                                                        @endcan
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                @endforeach
                                            </li>
                                        @endforeach


                                    </ul>

                                </div>

                            </div>
                        </form>

                        @can('add', Voyager::model('Setting'))

                            <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                                <h5 class="title">
                                    {{ __('voyager::settings.new') }}
                                </h5>
                            </div>
                            <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                                <div class="box">
                                    <form action="{{ route('voyager.settings.store') }}" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="setting_tab" class="setting_tab"
                                               value="{{ $active }}"/>
                                        <div class="row">
                                            <div class="margin-bottom padding-item col-lg-12 col-md-12 col-sm-12">
                                                <div class="input-row">
                                                    <label>
                                                        {{ __('voyager::generic.name') }}
                                                    </label>
                                                    <input type="text" class="form-control" name="display_name"
                                                           placeholder="{{ __('voyager::settings.help_name') }}"
                                                           required="required">

                                                </div>
                                            </div>
                                            <div class="margin-bottom padding-item col-lg-12 col-md-12 col-sm-12">
                                                <div class="input-row">
                                                    <label>
                                                        {{ __('voyager::generic.key') }}
                                                    </label>
                                                    <input type="text" class="form-control" name="key"
                                                           placeholder="{{ __('voyager::settings.help_key') }}"
                                                           required="required">

                                                </div>
                                            </div>
                                            <div class="margin-bottom padding-item col-lg-12 col-md-12 col-sm-12">
                                                <div class="input-row">
                                                    <label>
                                                        {{ __('voyager::generic.type') }}
                                                    </label>
                                                    <div class="dropdown custom-selects">
                                                        <select name="type" class="form-control" required="required">
                                                            <option
                                                                value="">{{ __('voyager::generic.choose_type') }}</option>
                                                            <option
                                                                value="text">{{ __('voyager::form.type_textbox') }}</option>
                                                            <option
                                                                value="text_area">{{ __('voyager::form.type_textarea') }}</option>
                                                            <option
                                                                value="rich_text_box">{{ __('voyager::form.type_richtextbox') }}</option>
                                                            <option
                                                                value="code_editor">{{ __('voyager::form.type_codeeditor') }}</option>
                                                            <option
                                                                value="checkbox">{{ __('voyager::form.type_checkbox') }}</option>
                                                            <option
                                                                value="radio_btn">{{ __('voyager::form.type_radiobutton') }}</option>
                                                            <option
                                                                value="select_dropdown">{{ __('voyager::form.type_selectdropdown') }}</option>
                                                            <option
                                                                value="file">{{ __('voyager::form.type_file') }}</option>
                                                            <option
                                                                value="image">{{ __('voyager::form.type_image') }}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="margin-bottom padding-item col-lg-12 col-md-12 col-sm-12">
                                                <div class="input-row">
                                                    <label>
                                                        {{ __('voyager::settings.group') }}
                                                    </label>
                                                    <div class="dropdown custom-selects">
                                                        <select class="form-control" name="group">
                                                            <option>{{ __('voyager::settings.group') }}</option>
                                                            @foreach($groups as $group)
                                                                <option value="{{ $group }}">{{ $group }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="padding-item margin-bottom col-lg-8 col-md-12 col-sm-12">
                                                <div class="add-row nav-button-row doc-button-row">
                                                    <button class="flex-box add-new">
                                                        <img src="{{voyager_asset('icon/Plus,%20Add.svg')}}">
                                                        {{ __('voyager::settings.add_new') }}
                                                    </button>

                                                </div>
                                            </div>

                                        </div>
                                    </form>

                                </div>
                            </div>
                        @endcan

                    </div>
                </div>
                {{--
                                <div class="col-lg-4 col-md-12 col-sm-12">
                                    <div class="row">
                                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                                            <h5 class="title">
                                                راهنمای ورود اطلاعات
                                            </h5>
                                        </div>
                                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                                            <p class="Description">
                                                لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک
                                                است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط
                                                فعلی تکنولوژی
                                            </p>
                                        </div>
                                    </div>
                                </div>
                --}}
            </div>
        </div>
    </div>

    @can('delete', Voyager::model('Setting'))
        <div class="modal modal-danger fade show" tabindex="-1" id="delete_modal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <h5 class="title">

                            {!! __('voyager::settings.delete_question', ['setting' => '<span id="delete_setting_title"></span>']) !!}
                        </h5>
                    </div>
                    <div class="modal-footer">
                        <form action="#" id="delete_form" method="POST">
                            {{ method_field("DELETE") }}
                            {{ csrf_field() }}
                            <button type="submit" class="flex-box add-new delete-confirm" style="font-size: 18px">

                                {{ __('voyager::settings.delete_confirm') }}

                            </button>
                        </form>

                        <button type="button" class="menu-items close" style="font-size: 18px" data-dismiss="modal">
                            {{ __('voyager::generic.cancel') }}</button>
                    </div>
                </div>
            </div>
        </div>
    @endcan

    <script>
        class Tabs {
            constructor(element) {
                this.tabs = element;
                this.toggles = this.tabs.querySelectorAll('.tabs__toggle');
                this.panels = this.tabs.querySelectorAll('.tabs__tab-panel')
            }

            init() {
                this.toggles.forEach(toggle => {
                    toggle.addEventListener('click', (e) => {
                        this.toggles.forEach(toggle => {
                            toggle.classList.remove('active');
                        })
                        this.panels.forEach(panel => {
                            panel.classList.remove('active');
                        })
                        e.target.classList.add('active');
                        this.tabs.querySelector(`.tabs__tab-panel[data-tab='${e.target.dataset.tab}']`).classList.add('active')
                    })
                })
            }
        }

        document.querySelectorAll('.tabs').forEach(tab => {
            const tabs = new Tabs(tab);
            tabs.init();
        })
    </script>

    <script>
        $(".account-number").click(function () {
            if ($(this).attr('title') == 'حذف') {
                var idModal = "/" + $(this).attr("data-id");
                $("#delete_form").attr('action', window.location.href + idModal)
            }
        });


        $('#upload-file-btn').on('change', function (e) {
            var fileName = e.target.files[0].name;

            $(".upload-file-row2").append("<div class=\"acsses  files\">" +
                "<div class=\"flex-box\">" +
                "<img class='close-tag' src=\"{{ voyager_asset('icon/clos.svg') }}\">"
                + fileName +
                "</div>" +
                "<div class=\"flex-box upload-prosses\">" +
                "</div>" +
                "</div>");
            $('.close-tag').click(function () {
                $(this).parent().parent().fadeOut();
            });

        });
        $('.delet-file').click(function () {
            $('.upload-file-row2 .files').fadeOut();
        });


        $('.upload-fileInput').on('change', function () {
            var filename = $(this).val().replace(/.*(\/|\\)/, '');
            var fileAppendBox = $(this).parents('.input-row').siblings(".upload-file-row");
            fileAppendBox.append("<div class=\"acsses  files\">" +
                "<div class=\"flex-box\">" +
                "<img class='close-tag' src=\"{{ voyager_asset('icon/clos.svg')}}\">"
                + filename +
                "</div>" +
                "<div class=\"flex-box upload-prosses\">" +
                "</div>" +
                "</div>");
            $('.close-tag').click(function () {
                $(this).parent().parent().fadeOut();
            });
        });

    </script>

@stop

@section('javascript')
    <script>
        $('document').ready(function () {
            $('#toggle_options').click(function () {
                $('.new-settings-options').toggle();
                if ($('#toggle_options .voyager-double-down').length) {
                    $('#toggle_options .voyager-double-down').removeClass('voyager-double-down').addClass('voyager-double-up');
                } else {
                    $('#toggle_options .voyager-double-up').removeClass('voyager-double-up').addClass('voyager-double-down');
                }
            });

            @can('delete', Voyager::model('Setting'))
            $('.panel-actions .voyager-trash').click(function () {
                var display = $(this).data('display-name') + '/' + $(this).data('display-key');

                $('#delete_setting_title').text(display);

                $('#delete_form')[0].action = '{{ route('voyager.settings.delete', [ 'id' => '__id' ]) }}'.replace('__id', $(this).data('id'));
                $('#delete_modal').modal('show');
            });
            @endcan

            $('.toggleswitch').bootstrapToggle();

            $('[data-toggle="tab"]').click(function () {
                $(".setting_tab").val($(this).html());
            });

            $('.delete_value').click(function (e) {
                e.preventDefault();
                $(this).closest('form').attr('action', $(this).attr('href'));
                $(this).closest('form').submit();
            });
        });
    </script>
    <script type="text/javascript">
        $(".group_select").not('.group_select_new').select2({
            tags: true,
            width: 'resolve'
        });
        $(".group_select_new").select2({
            tags: true,
            width: 'resolve',
            placeholder: '{{ __("voyager::generic.select_group") }}'
        });
        $(".group_select_new").val('').trigger('change');
    </script>
    <iframe id="form_target" name="form_target" style="display:none"></iframe>
    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="POST"
          enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
        {{ csrf_field() }}
        <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
        <input type="hidden" name="type_slug" id="type_slug" value="settings">
    </form>

    <script>
        var options_editor = ace.edit('options_editor');
        options_editor.getSession().setMode("ace/mode/json");

        var options_textarea = document.getElementById('options_textarea');
        options_editor.getSession().on('change', function () {
            console.log(options_editor.getValue());
            options_textarea.value = options_editor.getValue();
        });
    </script>
@stop
