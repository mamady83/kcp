<?php

namespace TCG\Voyager\Facades;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Facade;

class Notification extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Notification';
    }

    public static function notification()
    {
        return \TCG\Voyager\Models\Notification::getInstance();
    }

    public static function warning($value)
    {
        return \TCG\Voyager\Models\Notification::query()->create([
            'value' => $value,
            'type' => 'support'
            ]);
    }

    public static function success($value)
    {
        return \TCG\Voyager\Models\Notification::query()->create([
            'value' => $value,
            'type' => 'comment'
        ]);
    }

    public static function log($value)
    {
        return \TCG\Voyager\Models\Notification::query()->create([
            'value' => $value,
            'type' => 'client'
        ]);
    }

    public static function cart($value)
    {
        return \TCG\Voyager\Models\Notification::query()->create([
            'value' => $value,
            'type' => 'product'
        ]);
    }


}
